package Commands;

import Events.Main;
import SupportClass.SQLManager;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.sql.*;
import java.util.List;

public class ResendVerify extends ListenerAdapter {
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String[] args = event.getMessage().getContentRaw().split("\\s+");

        List<Role> roles = event.getMessage().getMember().getRoles();
        boolean hasNotVerifiedRole = false;
        for (Role r : roles) {
            if (r.getName().equals("not verified")) {
                hasNotVerifiedRole = true;
            }
        }

        if (args[0].equalsIgnoreCase(Main.prefix + "resendverify") && hasNotVerifiedRole == true) {
            if (args.length == 1) {
                String discordID = event.getAuthor().getId();
                String discordName = event.getAuthor().getAsTag();
                boolean verified = false;

                try (Connection connection = DriverManager.getConnection("jdbc:mysql://serwer1928401.home.pl:3306/30631776_aorlowski?serverTimezone=UTC", "30631776_aorlowski", "TNt#@7#Je3eh$N-?")) {
                    PreparedStatement verificationPlayer = connection.prepareStatement("SELECT * FROM players ORDER BY datetime DESC");
                    verificationPlayer.execute();
                    ResultSet verificationPlayerResultSet = verificationPlayer.getResultSet();

                    while (verificationPlayerResultSet.next()) {
                        String verificationPlayerNick = verificationPlayerResultSet.getString("osuname");
                        String verificationDiscordID = verificationPlayerResultSet.getString("discordid");
                        String verificationCode = verificationPlayerResultSet.getString("code");
                        String verificationIsVerified = verificationPlayerResultSet.getString("verified");
                        int verificationAttempts = verificationPlayerResultSet.getInt("attempt");


                        if (verificationAttempts == 3) {

                            event.getGuild().getController().addRolesToMember(event.getMember(), event.getGuild().getRolesByName("muted", false)).complete();
                            new java.util.Timer().schedule(
                                    new java.util.TimerTask() {
                                        @Override
                                        public void run() {
                                            event.getGuild().getController().removeRolesFromMember(event.getMember(), event.getGuild().getRolesByName("muted", false)).complete();
                                            SQLManager.resetAttempts(discordID);
                                        }
                                    },
                                    3600000
                            );
                            PreparedStatement unableToUseVerify = connection.prepareStatement("UPDATE players SET attempt=4 WHERE code=" + verificationCode);
                            unableToUseVerify.execute();
                            EmbedBuilder info = new EmbedBuilder();
                            info.setTitle("Command overuse! :anger:");
                            info.setDescription(event.getAuthor().getAsMention() + " has been muted for 1 hour due to overusing IRC command!");
                            info.setColor(0xff0087);
                            event.getChannel().sendMessage(info.build()).queue();
                            verified = true;
                            break;
                        } else if (verificationAttempts == 4) {
                            verified = true;
                            break;
                        }
                        if (verificationDiscordID.equals(discordID) && verificationIsVerified.equals("no")) {
                            try {
//                                System.out.println("Waiting 10s");
//                                Thread.sleep(10000);
                                verificationAttempts = verificationAttempts + 1;
                                PreparedStatement verificationAttempt = connection.prepareStatement("UPDATE players SET attempt=" + verificationAttempts + " WHERE code=" + verificationCode);
                                verificationAttempt.execute();
                                Main irc = new Main();
                                irc.setName(verificationPlayerNick);
                                irc.setCode(verificationCode);
                                irc.sendCode();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            EmbedBuilder info = new EmbedBuilder();
                            info.setTitle("Verification process...");
                            info.setDescription(event.getAuthor().getAsMention() + ", verification code has been sent to **" + verificationPlayerNick + "**");
                            info.setFooter("Attempts: " + verificationAttempts + "/3", event.getMember().getUser().getAvatarUrl());
                            info.setColor(0xa921c4);
                            event.getChannel().sendMessage(info.build()).queue();
                            verified = true;
                            break;
                        } else if (hasNotVerifiedRole == false) {
                            EmbedBuilder info = new EmbedBuilder();
                            info.setTitle("Verification process...");
                            info.setDescription(event.getAuthor().getAsMention() + ", are you trying to break me? I'm not that soft! You are already verified!");
                            info.setColor(0xff0087);
                            event.getChannel().sendMessage(info.build()).queue();
                            verified = true;
                            break;
                        }
                    }
                    if (verified == false) {
                        EmbedBuilder info = new EmbedBuilder();
                        info.setTitle("Verification process...");
                        info.setDescription(event.getAuthor().getAsMention() + ", are you trying to break me? I'm not that soft! You didn't even try to verify or you changed your discord nick while being in verification process!");
                        info.setColor(0xff0087);
                        event.getChannel().sendMessage(info.build()).queue();
                    }

                } catch (SQLException e) {
                    EmbedBuilder info = new EmbedBuilder();
                    info.setTitle("SQL Error");
                    info.setDescription("Unable to connect to database!");
                    info.setColor(0xff0087);
                    event.getChannel().sendMessage(info.build()).queue();
                    e.printStackTrace();
                }
            }
            if (args.length >= 2) {
                String discordID = event.getAuthor().getId();
                StringBuilder osuName = new StringBuilder();
                boolean verified = false;

                for (int i = 0; i < args.length; i++) {
                    if (i == args.length - 1) {
                        break;
                    }
                    osuName.append(args[i + 1]);
                    osuName.append(" ");
                }

                try (Connection connection = DriverManager.getConnection("jdbc:mysql://serwer1928401.home.pl:3306/30631776_aorlowski?serverTimezone=UTC", "30631776_aorlowski", "TNt#@7#Je3eh$N-?")) {
                    PreparedStatement verificationPlayer = connection.prepareStatement("SELECT * FROM players");
                    verificationPlayer.execute();

                    ResultSet verificationPlayerResultSet = verificationPlayer.getResultSet();

                    while (verificationPlayerResultSet.next()) {
                        String verificationDiscordID = verificationPlayerResultSet.getString("discordid");
                        String verificationCode = verificationPlayerResultSet.getString("code");
                        String verificationOsuName = verificationPlayerResultSet.getString("osuname");
                        String verificationIsVerified = verificationPlayerResultSet.getString("veryfied");

                        if (hasNotVerifiedRole == false) {

                            EmbedBuilder info = new EmbedBuilder();
                            info.setTitle("Verification process...");
                            info.setDescription(event.getAuthor().getAsMention() + ", are you trying to break me? I'm not that soft! You are already verified!");
                            info.setColor(0xff0087);
                            event.getChannel().sendMessage(info.build()).queue();
                            verified = true;
                            break;
                        }
                        if (verificationDiscordID.equals(discordID) && verificationOsuName.equals(osuName.toString()) && verificationIsVerified.equals("no")) {
//                            try {
//                                System.out.println("waiting 10s");
//                                Thread.sleep(10000);

                                Main irc = new Main();
                                irc.setName(osuName.toString());
                                irc.setCode(verificationCode);
                                irc.sendCode();

                                EmbedBuilder info = new EmbedBuilder();
                                info.setTitle("Verification process...");
                                info.setDescription(event.getAuthor().getAsMention() + ", verification code has been sent to **" + osuName.toString() + "**. If it's not your osu account, use *>verify <osuName>* again.");
                                info.setColor(0xa921c4);
                                event.getChannel().sendMessage(info.build()).queue();
                                verified = true;
                                break;
//                            } catch (InterruptedException e) {
//                                e.printStackTrace();
//                            }
                        }
                    }
                    if (verified == false) {
                        EmbedBuilder info = new EmbedBuilder();
                        info.setTitle("Verification process...");
                        info.setDescription(event.getAuthor().getAsMention() + ", are you trying to break me? I'm not that soft! You didn't even try to verify!");
                        info.setColor(0xff0087);
                        event.getChannel().sendMessage(info.build()).queue();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } else if (args[0].equalsIgnoreCase(Main.prefix + "resendverify") && hasNotVerifiedRole == false) {
        }
    }
}