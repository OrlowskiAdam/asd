package Commands;

import Events.Main;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.sql.*;
import java.util.Arrays;

public class ShowNextMatches extends ListenerAdapter {
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String[] args = event.getMessage().getContentRaw().split("\\s+");
        if (args[0].equalsIgnoreCase(Main.prefix + "showNextMatches")) {
            try (Connection connection = DriverManager.getConnection("jdbc:mysql://serwer1928401.home.pl:3306/30631776_aorlowski?serverTimezone=UTC", "30631776_aorlowski", "TNt#@7#Je3eh$N-?")) {

                PreparedStatement selectMatchesStatement = connection.prepareStatement("SELECT * FROM matches WHERE datetime > CURDATE() ORDER BY datetime");
                selectMatchesStatement.execute();
                ResultSet selectMatchesStatementResultSet = selectMatchesStatement.getResultSet();

                String teamA;
                String teamB;
                String datetime;
                String referee;
                String[] result = new String[0];

                while (selectMatchesStatementResultSet.next()) {
                    teamA = selectMatchesStatementResultSet.getString("teamA").toUpperCase();
                    teamB = selectMatchesStatementResultSet.getString("teamB").toUpperCase();
                    datetime = selectMatchesStatementResultSet.getString("datetime");
                    referee = selectMatchesStatementResultSet.getString("referee");

                    String operation = "**" + teamA + "** vs. **" + teamB + "** at **" + datetime + "** referee: **" + referee + "**";

                    result = Arrays.copyOf(result, result.length + 1);
                    result[result.length - 1] = operation;

                }
                String matches = String.join("\n", result);

                EmbedBuilder info = new EmbedBuilder();

                info.setTitle("Next matches:");
                info.setDescription(matches);
                info.setColor(0xa921c4);
                event.getChannel().sendMessage(info.build()).queue();

            } catch (SQLException e) {
                e.printStackTrace();
            }



        }
    }
}
