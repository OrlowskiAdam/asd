package Commands;

import org.pircbotx.Configuration;
import org.pircbotx.PircBotX;
import org.pircbotx.exception.IrcException;

import java.io.IOException;

class IRC extends org.pircbotx.hooks.ListenerAdapter {
    private static PircBotX bot;
    private static String osuName;
    private static String code;

    public void setName(String name) {
        this.osuName = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

//    @Override
//    public void onConnect(ConnectEvent event) {
//        bot.sendIRC().message("" + osuName, "Your verification code is: " + code + ". If you see this message and you are not trying to verify to any tournament, please ignore this message. If you receive them again, add my account to ignore list.");
//        bot.sendIRC().quitServer();
//    }

    public static void sendCode() {
        bot.sendIRC().message("" + osuName, "Your verification code is: " + code + ". If you see this message and you are not trying to verify to any tournament, please ignore this message. If you receive them again, add my account to ignore list.");
    }

    public void IRC() {
        Configuration configuration = new Configuration.Builder()
                .setName("desireofpainx")
                .setServerHostname("irc.deltaanime.net")
                .setServerPort(6667)
//                .setServerPassword("87151043")
                .addAutoJoinChannel("#general")
                .addListener(new IRC())
                .buildConfiguration();

        bot = new PircBotX(configuration);
        try {
            bot.startBot();
        } catch (IOException | IrcException e) {
            e.printStackTrace();
        }

    }
}