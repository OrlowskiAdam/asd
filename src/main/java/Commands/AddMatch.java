package Commands;

import Events.Main;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AddMatch extends ListenerAdapter {
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String[] args = event.getMessage().getContentRaw().split("\\s+");
        if (args[0].equalsIgnoreCase(Main.prefix + "AddMatch")) {
            try (Connection connection = DriverManager.getConnection("jdbc:mysql://serwer1928401.home.pl:3306/30631776_aorlowski?serverTimezone=UTC", "30631776_aorlowski", "TNt#@7#Je3eh$N-?")) {

                if (args.length < 2) {
                    EmbedBuilder error = new EmbedBuilder();
                    error.setTitle("Error:");
                    error.setDescription("No arguments given.");
                    error.setColor(0xd83a29);
                    event.getChannel().sendMessage(error.build()).queue();
                } else if (args[1].equals("teamA") && args[3].equals("teamB") && args[5].equals("date") && args[7].equals("time") && args[9].equals("ref")) {
                    PreparedStatement addMatchesStatement = connection.prepareStatement("INSERT INTO matches (teamA, teamB, datetime, referee) VALUES (?, ?, ?, ?)");

                    String datetime = args[6] + " " + args[8];

                    addMatchesStatement.setString(1, args[2]);
                    addMatchesStatement.setString(2, args[4]);
                    addMatchesStatement.setString(3, datetime);
                    addMatchesStatement.setString(4, args[10]);
                    addMatchesStatement.execute();

                    EmbedBuilder success = new EmbedBuilder();
                    success.setTitle("Success!");
                    success.setDescription("**" + args[2] + "** vs. **" + args[4] + "** at **" + datetime + "** refereed by **" + args[10] + "** added successfuly!");
                    success.setColor(0xa921c4);
                    event.getChannel().sendMessage(success.build()).queue();
                } else {
                    EmbedBuilder error = new EmbedBuilder();
                    error.setTitle("Error:");
                    error.setDescription("Wrong arguments!");
                    error.setColor(0xd83a29);
                    event.getChannel().sendMessage(error.build()).queue();
                }
//
//
//
//                PreparedStatement selectMatchesStatement = connection.prepareStatement("SELECT * FROM matches");
//                selectMatchesStatement.execute();
//                ResultSet selectMatchesStatementResultSet = selectMatchesStatement.getResultSet();
//
//                int id;
//                String teamA;
//                String teamB;
//                String datetime;
//                String[] result = new String[0];
//
//                while (selectMatchesStatementResultSet.next()) {
//                    id = selectMatchesStatementResultSet.getInt("id");
//                    teamA = selectMatchesStatementResultSet.getString("teamA");
//                    teamB = selectMatchesStatementResultSet.getString("teamB");
//                    datetime = selectMatchesStatementResultSet.getString("datetime");
//
//                    String operation = "**Match id:** " + id + " = = = **" + teamA + "** vs. **" + teamB + "**   at **" + datetime + "**UTC+0";
//
//                    result = Arrays.copyOf(result, result.length + 1);
//                    result[result.length - 1] = operation;
//
//                }
//                String matches = String.join("\n", result);
//
//                EmbedBuilder info = new EmbedBuilder();
//
//                info.setTitle("Next matches:");
//                info.setDescription(matches);
//                info.setColor(0xa921c4);
//                event.getChannel().sendMessage(info.build()).queue();

            } catch (SQLException e) {
                e.printStackTrace();
            }



        }
    }
}
