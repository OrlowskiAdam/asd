package Commands;

import Events.Main;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.sql.*;
import java.util.List;
import java.util.Random;
import java.time.LocalDateTime;
import java.sql.Timestamp;

public class Verify extends ListenerAdapter {

    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String[] args = event.getMessage().getContentRaw().split("\\s+");
        StringBuilder osuName = new StringBuilder();
        LocalDateTime now = LocalDateTime.now();
        Timestamp sqlNow = Timestamp.valueOf(now);
        List<Role> roles = event.getMessage().getMember().getRoles();
        boolean hasNotVerifiedRole = false;
        for (Role r : roles) {
            if (r.getName().equals("not verified")) {
                hasNotVerifiedRole = true;
            }
        }

        if (args[0].equalsIgnoreCase(Main.prefix + "verify") && hasNotVerifiedRole == true) {

            //Jeśli przy komendzie verify nie podał żadnego nicku
            if (args.length == 1) {
                EmbedBuilder error = new EmbedBuilder();
                error.setTitle("Error:");
                error.setDescription("You have to give me your osu! name!");
                error.setColor(0xff0087);
                event.getChannel().sendMessage(error.build()).queue();

                //Jeśli próbuje się zweryfikować: verify <osunick> wysyła koda na osu!
            } else if (args.length > 1) {
                String discordID = event.getAuthor().getId();
                String discordName = event.getAuthor().getAsTag();

                //Łączy nick osu
                for (int i = 0; i < args.length; i++) {
                    if (i == args.length - 1) {
                        break;
                    }
                    osuName.append(args[i + 1]);
                    osuName.append(" ");
                }

                //Tworzy sześciocyfrowy kod
                Random random = new Random();
                int randomCode = 0;
                int[] numbers = new int[6];
                for (int i = 0; i < 6; i++) {
                    numbers[i] = random.nextInt(8) + 1;
                }
                for (int i = 0; i < numbers.length; i++) {
                    if (i == 5) {
                        randomCode = randomCode + numbers[i];
                    } else {
                        randomCode = (randomCode + numbers[i]) * 10;
                    }
                }
                String code = Integer.toString(randomCode);

                try (Connection connection = DriverManager.getConnection("jdbc:mysql://serwer1928401.home.pl:3306/30631776_aorlowski?serverTimezone=UTC", "30631776_aorlowski", "TNt#@7#Je3eh$N-?")) {
                    //Liczy ile razy uzytkownik próbował uzyć komendy >verify
                    PreparedStatement discordIDCount = connection.prepareStatement("SELECT COUNT(discordid) FROM players WHERE discordid=" + discordID);
                    discordIDCount.execute();
                    ResultSet discordIDCountResultSet = discordIDCount.getResultSet();
                    boolean verify = false;
                    int discordIDTotal = 0;
                    while (discordIDCountResultSet.next()) {
                        discordIDTotal = discordIDCountResultSet.getInt("COUNT(discordid)");
                    }

                    //Jeśli uzył juz 3 razy to dostaje mute na 1h
                    if (discordIDTotal == 3) {
                        event.getGuild().getController().addRolesToMember(event.getMember(), event.getGuild().getRolesByName("muted", false)).complete();
                        new java.util.Timer().schedule(
                                new java.util.TimerTask() {
                                    @Override
                                    public void run() {
                                        event.getGuild().getController().removeRolesFromMember(event.getMember(), event.getGuild().getRolesByName("muted", false)).complete();
                                    }
                                },
                                3600000
                        );
                        EmbedBuilder info = new EmbedBuilder();
                        info.setTitle("Command overuse! :anger:");
                        info.setDescription(event.getAuthor().getAsMention() + " has been muted for 1 hour!");
                        info.setColor(0xff0087);
                        event.getChannel().sendMessage(info.build()).queue();
                        verify = true;
                    } else if (discordIDTotal > 3) {
                    } else {
                        //Jeśli wszystko jest ok i uzytkownik moze uzyć komendy >verify
                        PreparedStatement isPlayerVerified = connection.prepareStatement("SELECT * FROM players");
                        isPlayerVerified.execute();
                        ResultSet isPlayerVerifiedResultSet = isPlayerVerified.getResultSet();
                        //Sprawdza czy juz wcześniej uzył komendy >verify
                        while (isPlayerVerifiedResultSet.next()) {
                            String isDiscordID = isPlayerVerifiedResultSet.getString("discordid");
                            String isOsuName = isPlayerVerifiedResultSet.getString("osuname");
                            String isCode = isPlayerVerifiedResultSet.getString("code");

                            //Jeśli uzył, to informuje uzytkownika, zeby uzył komendy >resendverify
                            if (isDiscordID.equals(discordID) && isOsuName.equals(osuName.toString())) {
                                EmbedBuilder info = new EmbedBuilder();
                                info.setTitle("Verification process...");
                                info.setDescription(event.getAuthor().getAsMention() + ", looks like you were trying to verify, but you didn't finish verification process. Type **>resendverify** and I will send your verification code again.");
                                info.setColor(0xff0087);
                                event.getChannel().sendMessage(info.build()).queue();
                                verify = true;
                                break;
                            }
                            //Jeśli podał kod >verify [kod] to zostanie zweryfikowany
                            if (isDiscordID.equals(discordID) && isCode.equals(args[1])) {
                                PreparedStatement verificationPlayer = connection.prepareStatement("UPDATE players SET verified='yes' WHERE code=" + args[1]);
                                verificationPlayer.execute();

                                EmbedBuilder info = new EmbedBuilder();
                                info.setTitle("Verification process...");
                                info.setDescription(event.getAuthor().getAsMention() + ", your osu! account **" + isOsuName + "** has been successfully connected to discord! You are verified now :)");
                                info.setColor(0xa921c4);
                                event.getChannel().sendMessage(info.build()).queue();

                                event.getGuild().getController().removeRolesFromMember(event.getMember(), event.getGuild().getRolesByName("not verified", false)).complete();

                                verify = true;
                                break;
                            }
                        }
                        //Jeśli uzytkownik nie oscyluje jeszcze w bazie, to go doda razem z kodem
                        if (verify == false) {
                            PreparedStatement verificationPlayer = connection.prepareStatement("INSERT INTO players (discordname, discordid, osuname, code, verified, datetime, attempt) VALUES (?, ?, ?, ?, ?, ?, ?)");

                            verificationPlayer.setString(1, discordName);
                            verificationPlayer.setString(2, discordID);
                            verificationPlayer.setString(3, osuName.toString());
                            verificationPlayer.setString(4, code);
                            verificationPlayer.setString(5, "no");
                            verificationPlayer.setTimestamp(6, sqlNow);
                            verificationPlayer.setInt(7, 0);
                            verificationPlayer.execute();

                            Main irc = new Main();
                            irc.setName(osuName.toString());
                            irc.setCode(code);
                            irc.sendCode();
                            discordIDTotal = discordIDTotal + 1;
                            EmbedBuilder info = new EmbedBuilder();
                            info.setTitle("Verification process...");
                            info.setDescription(event.getAuthor().getAsMention() + ", your verification code has been sent on osu!");
                            info.setFooter("Attempts: " + discordIDTotal + "/3", event.getMember().getUser().getAvatarUrl());
                            info.setColor(0xa921c4);
                            event.getChannel().sendMessage(info.build()).queue();

                        }
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            //Jeśli uzytkownik jest juz zweryfikowany
        } else if (args[0].equalsIgnoreCase(Main.prefix + "verify") && hasNotVerifiedRole == false) {
        }
    }
}