package Events;

import Commands.*;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.entities.Game;
import org.pircbotx.Configuration;
import org.pircbotx.PircBotX;
import org.pircbotx.exception.IrcException;
import org.pircbotx.hooks.events.DisconnectEvent;
import javax.security.auth.login.LoginException;
import java.io.IOException;

public class Main extends org.pircbotx.hooks.ListenerAdapter {
    private static PircBotX bot;
    private static String osuName;
    private static String code;
    public static JDA jda;
    public static String prefix = ">";

    public void setName(String name) {
        this.osuName = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static void sendCode() {
        bot.sendIRC().message("" + osuName, "Your verification code is: " + code + ". If you see this message and you are not trying to verify to any tournament, please ignore this message. If you receive them again, add my account to ignore list.");
    }

    @Override
    public void onDisconnect(DisconnectEvent event) {
        try {
            bot.startBot();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IrcException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws LoginException {
        jda = new JDABuilder(AccountType.BOT).setToken("NTc5MjY1MDEwMzcxNjU3NzQy.XO1x9A.sYNh1ChhgSaXdb0cB0-WjUkm25g").build();
        jda.getPresence().setStatus(OnlineStatus.ONLINE);


        jda.addEventListener(new ShowAllMatches());
        jda.addEventListener(new ShowNextMatches());
        jda.addEventListener(new AddMatch());
        jda.addEventListener(new Verify());
        jda.addEventListener(new ResendVerify());
        jda.addEventListener(new SetNotVerifiedRoleAfterJoinEvent());

        Configuration configuration = new Configuration.Builder()
                .setName("desireofpainx")
                .setServerHostname("irc.deltaanime.net")
                .setServerPort(6667)
//                .setServerPassword("87151043")
                .addAutoJoinChannel("#general")
                .addListener(new Main())
                .buildConfiguration();

        bot = new PircBotX(configuration);
        try {
            bot.startBot();
        } catch (IOException | IrcException e) {
            e.printStackTrace();
        }
    }
}