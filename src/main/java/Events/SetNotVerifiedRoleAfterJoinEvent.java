package Events;

import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class SetNotVerifiedRoleAfterJoinEvent extends ListenerAdapter {
    public void onGuildMemberJoin(GuildMemberJoinEvent event) {
        event.getGuild().getController().addRolesToMember(event.getMember(), event.getGuild().getRolesByName("not verified", false)).complete();
    }
}